<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	public function __construct(){
		parent::__construct();
		
		$this->load->model('user_model');

		
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['users'] = $this->user_model->getAll();
		$this->load->view('home/header');
		$this->load->view('home/user_list', $data);
	}

	public function create($editID = null)
	{


		$isCreate = $this->input->post('isCreate');
		$id = $this->input->post('id');

		$data['user'] = $this->user_model->getById($editID);
		
		if($isCreate != null){
			$this->load->library('form_validation');
      		$this->form_validation->set_rules('new','required|matches[confirm]|min_length[8]|max_length[16]');

			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[cpassword]');
			$this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required');

			if ($this->form_validation->run()){
				$this->user_model->createOrUpdate($id);
				redirect("/home", "refresh");
			}
		}

		$this->load->view('home/header');
		$this->load->view('home/create_new_user', $data);
	}

	
	public function delete($id)
	{
		$this->user_model->delete($id);
		redirect("/home", "refresh");

	}

	

}
