
<div id = "body">
<?php echo form_open('home/create'); ?>

<?php echo validation_errors(); ?>

<table>
	<tr>
		<td><?php echo form_label('Username:', 'username'); ?></td>
		<td><?php echo form_input('username', ($user == null) ? "" : $user->username); ?></td>
	</tr>
	<tr>
		<td><?php echo form_label('Password:', 'password'); ?></td>
		<td><?php echo form_password('password', ($user == null) ? "" : $user->password); ?></td>
	</tr>
	<tr>
		<td><?php echo form_label('Confirm Password:', 'cpassword'); ?></td>
		<td><?php echo form_password('cpassword'); ?></td>
	</tr>
	<?php echo form_hidden('isCreate', 'true'); ?>
	<?php echo form_hidden('id', ($user == null) ? "" : $user->id); ?>


	<tr></tr>
	<tr>
		<td colspan="2" align="center"><?php echo form_submit('submit', 'Save!'); ?></td>
	</tr>
</table>

</div>