<?php 
class User_model extends CI_Model{
	public function __construct()
        {
                // Call the CI_Model constructor
                $this->load->database();
        }

        public function getAll()
        {
                $query = $this->db->get('users');
                return $query->result_array();
        }

        public function getById($id)
        {
        		$query = $this->db->get_where('users', array('id' => $id));
                return $query->row();
        }

        public function delete($id)
        {
                $this->db->delete('users', array('id' => $id)); 
        }

        public function createOrUpdate($id){
                $data = array(
                        'username' => $this->input->post('username'),
                        'password' => $this->input->post('password')
                    );

                if($id == null){      
                    return $this->db->insert('users', $data);
                }else{
                    $this->db->where('id', $id);
                    return $this->db->update('users', $data);              
                }
        }
}